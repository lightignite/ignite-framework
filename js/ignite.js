/*--------------------------------
	global variables
--------------------------------*/
var topSpacing = 0;

(function($){
  $(function(){

    /*--------------------------------
    	utility functions
    --------------------------------*/
    
      //activate fastclick on mobile browser
      //FastClick.attach(document.body);
      
      //activate foundation
      $(document).foundation();
      
       //remove empty sections of content
      $('.removeIfEmpty, .hideIfEmpty').each( function(){      
        if($(this).find('img, iframe, object').length === 0 && $.trim($(this).text()).length === 0){
          //no text content was found, hide element
          $(this).hide();
        }
      });
  
      //detect if admin bar is visible
      if($('.lcms_menus').length){
        $('html').addClass('adminBarPresent');
        topSpacing = $('.lcms_menus').innerHeight();
      }
      
      //apply element classes to title as well
      $('.container').each(function(){
        $titleContainer = $(this);
        $next = $titleContainer.next();
        
        if($next.hasClass('element')){
          var nextClasses = $next.attr('class');
          var nextClassesArray = nextClasses.split(' ');
          
          var arrayLength = nextClassesArray.length;

          for (var i = 0; i < arrayLength; i++) {
            if(nextClassesArray[i] != "element"){
              $titleContainer.addClass( nextClassesArray[i] );              
            }
          }
        }
      });
      
    /*----*/



    /*--------------------------------
    	resize behavior
    --------------------------------*/
      
      //set current window width & height
      var lastWidth = $(window).width();
      var lastHeight = $(window).height();
      
      //fire resizeFunctions immediately
      resizeFunctions();
      
      //fire resizeFunctions again when page finished loading
      $(window).on('load', function() {
        resizeFunctions();
      });
      
      //fire resizeFunctions if window resized or rotated
      $(window).resize(function(){
        if(($(this).width() != lastWidth) || ($(this).height() != lastHeight)){
          lastWidth = $(this).width();
          lastHeight = $(this).height();
          resizeFunctions();  
        }    
      });
    
    /*----*/
  
  
    /*--------------------------------
    	mobile menu
    --------------------------------*/
    
      $('.menuTrigger').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.mobileMenu').slideToggle(150);
      });
      
      $('.mobileMenu li.hasChildren').each(function(){
    
        //LightCMS adds "hasChildren" class even if children aren't displayed in current nav menu. 
        //Therefore we need to check and make sure the child UL is actually present before adding toggle.
        
        if($(this).find(' > ul').length){ 
          $(this).find('> a').append('<span class="submenuToggle"><i class="toggleIcon"></i></span>');
        }
      });
      
      $('.submenuToggle').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $parentLi = $(this).closest('li');
        $parentLi.toggleClass('active');
        $parentLi.find('> ul').slideToggle(150);
      });

      $('.mobileMenu a').click(function(e){
        if($(this).attr('href') == "#"){
          e.preventDefault();
          $parentLi = $(this).closest('li');
          $parentLi.toggleClass('active');
          $parentLi.find('> ul').slideToggle(150);
        }      
      });
    
      //prevent mobile menu from expanding off screen  
      $(window).on("load, resize, scroll", function () {
        $('.mobileMenu').css('max-height', function(){
          //get max height of menu by subtracting menu container's height and lcms top menu spacing (to derive .mobileMenu's position from top)
          return $(window).height() - $('.is-sticky').height() - topSpacing+1;
        });
      });

    /*---*/  
  
  
    /*--------------------------------
    	calendar
    --------------------------------*/
    
      setupResponsiveCalendar();
      
      //calendar needs reset every time the month switches, which happens
      //when the next/prev links are clicked
      
      $('body').on('click', '.date-picker-select a', function(){
  
        //get HTML of current table content (TR's 3+)
        var oldTableContent = "";      
        var $eventsCalendar = $(this).closest('.lcms-calendar');      
        
        $eventsCalendar.find('.lcms-calendar__table').each(function() {
           oldTableContent += $(this).html();
        });
        //console.log(oldTableContent);
        
        //set up timer to watch current table content and compare to old table content until a change is detected
        var tableContentWatcher = setInterval(function(){
          console.log('timer run');
          var currentTableContent = "";
          
          $eventsCalendar.find('.lcms-calendar__table').each(function() {
             currentTableContent += $(this).html();
          });
                  
          if(currentTableContent != oldTableContent){
            console.log('table content changed');
            clearInterval(tableContentWatcher);
            setupResponsiveCalendar();     
          } 
        }, 100); //checks your content box all 10 seconds and triggers alert when content has changed... 
      });      
      
    /*----*/

    
    /*--------------------------------
    	use image gallery descriptions
    	as links
    --------------------------------*/
    
      $('.descriptionsAsLinks').find('.thumbContents').each(function(){
        var linkURL = $(this).find(" > .description").text();
        $(this).find(" > a").attr('href', linkURL.trim());
      });
      
      $('.disableLightbox').find('.thumbContents').each(function(){
        $(this).find(" > a").removeAttr('href');
      });
    
    /*---*/

    /*--------------------------------
    	webforms
    --------------------------------*/
      //close lightbox when clicking outside message, on "return to previous page" link, or "x" in the corner
      $('body').on('click', '#FormBuilderLightBox, .confirmation-content-message a[href="javascript:location.href = location.href;"]', function(e){
        $('#FormBuilderLightBox, #lightbox_content').hide();
        e.preventDefault();
        return false;
      });
    /*---*/



    //execute on page load
    $(window).on("load", function () {
      $("body").removeClass("preload");
    });



  });	
})(jQuery);


/*---------------------------------------------
-----------------------------------------------
	resizeFunctions()
	
	Add any JS here that needs to be
	executed every time browser 
	window size changes. Make sure
	JS is performant (since it may
	be executed many times) and self
	contained.
-----------------------------------------------
---------------------------------------------*/

function resizeFunctions(){
  console.log('resized');
  
  
  /*--------------------------------
    'more' menu
  --------------------------------*/
	
	$('ul.moreMenu').each(function(){
  	var $moreMenu = $(this);
  	var moreMenuItems = new Array();
    
    //continue only if menu is visible
  	if ($moreMenu.is(':visible')){ 
    	
    	//remove menu item wrapper, link, and list
    	$('.moreMenuWrapper, .moreMenuItemList').contents().unwrap();
    	$('.moreMenuItemLink').remove();
    	
    	var menuWidth = $moreMenu.parent().width();
    	var menuItemTotalWidth = 0;
    	
    	//console.log('menu width:' + menuWidth);
    	
    	$moreMenu.find('>li').each(function() {

      	//if menu item is visible, add its width to total
        if ($(this).is(':visible')){
          menuItemTotalWidth += $(this).outerWidth();
        }
      });
      
    	//console.log('menu items total width:' + menuItemTotalWidth);
      
      //rebuild menu if items are wider than menu (and thus wrapping)
      if(menuItemTotalWidth > menuWidth){
        
        //add "more" container and include it in total item width
        $moreMenuWrapper = $('<li class="hasChildren moreMenuWrapper"><a href="/#" class="moreMenuItemLink">More</a><ul class="level2 moreMenuItemList"></ul></li>');
        $moreMenu.append($moreMenuWrapper);
        
        menuItemTotalWidth = menuItemTotalWidth + $moreMenuWrapper.outerWidth();
        
        //start looping through menu items in reverse order
        $($moreMenu.find('> li:visible').not('.moreMenuWrapper').get().reverse()).each(function() {
          var $moreMenuItem = $(this);
          
          //add the menu item to the array of items that will be hidden later
          moreMenuItems.push($moreMenuItem); //could push a clone instead if this will goof order of hidden menu items for any reason.
          
          menuItemTotalWidth = menuItemTotalWidth - $moreMenuItem.outerWidth();
          
          if(menuWidth >= menuItemTotalWidth){
            //if width of all menu items is less than total menu width then exit the .each loop
            return false;
          }         
        });
        
      	//moreMenuItems were gathered in reverse order, so need to turn them around before putting back into navigation
        moreMenuItems.reverse();
        
        $('.moreMenuItemList').append(moreMenuItems);
      }
  	}
	});
}
/*----*/

/*---------------------------------------------
-----------------------------------------------
	setupResponsiveCalendar()
	
	Parses current calendar data
	and adds "hasEvents" class to 
	days containing event info.
-----------------------------------------------
---------------------------------------------*/

function setupResponsiveCalendar(){  
  $('.day-of-week').each(function(){
    if($(this).find('a').length > 0){
      $(this).addClass('hasEvents');
    }
  });
}
/*----*/