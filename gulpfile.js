/*--------------------------------
	packages
--------------------------------*/

var gulp            = require('gulp');
var rename          = require('gulp-rename');
var concat          = require('gulp-concat');
var filter          = require('gulp-filter');  
var mainBowerFiles  = require('main-bower-files');
var gutil           = require('gulp-util');
var notify          = require("gulp-notify");
var plumber         = require("gulp-plumber");
var replace         = require('gulp-replace');
var gitWatch        = require('gulp-git-watch');

// js
var uglify          = require('gulp-uglify');

// css
var sass            = require('gulp-sass');
//var minify          = require('gulp-clean-css');     //https://github.com/jakubpawlowicz/clean-css#how-to-use-clean-css-programmatically
var prefix          = require('gulp-autoprefixer');


/*--------------------------------
	custom functions
--------------------------------*/

function string_src(filename, string) {
  var src = require('stream').Readable({ objectMode: true })
  src._read = function () {
    this.push(new gutil.File({ cwd: "", base: "", path: filename, contents: new Buffer(string) }))
    this.push(null)
  }
  return src
}

var onError = function (err) {  
  //notify.onError("Error: <%= err.message %>")
  gutil.beep();
  notify(err);
  console.log(err.messageFormatted);
  this.emit('end');
};


/*--------------------------------
	config
--------------------------------*/

//set input directories
var config = {
  
  js: {
    main:         'src/js/ignite.js',
    libs:         'src/js/libs/**/*.js',
    dest:         'templates/ignite/js'
  },
  
  css: {
    src:          ['src/scss/**/*.scss', '!src/scss/texteditor.scss'],
    main:         'src/scss/main.scss',
    texteditor:   'src/scss/texteditor.scss',
    dest:         'templates/ignite/css'
  },
    
  bower:          ['bower.json', '.bowerrc']
}


/*--------------------------------
	tasks
--------------------------------*/

//default task
gulp.task('default', ['build']);

/*--------------------------------
	watch tasks
--------------------------------*/

gulp.task('watch',function() { 
  /* check remote repository against local every X minutes to ensure
   a new version hasn't been published while you're working */
  gitWatch({poll:1000*60*5})
  	.on('nochange', function() {
  		//console.log('GitWatch - Local repository is latest version');
  	})
  	.on('change', function(newHash, oldHash) {
  		notify({ message: 'NEW UPDATES FOUND IN REMOTE REPOSITORY!' })
  		//console.log('CHANGES! FROM', oldHash, '->', newHash);
  	});
  
  gulp.watch(config.css.src,['css-main']);
  
  gulp.watch(config.css.texteditor,['css-texteditor']);
  gulp.watch(config.js.main,['js-main']);
  gulp.watch(config.js.libs,['js-libs']);
});


  
/*--------------------------------
	build tasks
--------------------------------*/  

//execute all build scripts

gulp.task('build', [
  'css-main',
  'css-texteditor',
  'js-libs',
  'js-main'
]); 


//generate main.css and main.min.css
gulp.task('css-main', function () {
  gulp.src(config.css.main)
    .pipe(plumber({errorHandler: notify.onError({title:"css-main error", message: "<%= error.message %>"})}))
    .pipe(sass({
      errLogToConsole: false
    }))
    .pipe(prefix({
      browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
			cascade: true,
			remove: true
		}))
    .pipe(gulp.dest(config.css.dest))
    .pipe(notify({ message: 'Build main.css complete' }));
});

gulp.task('css-texteditor', function () {
  gulp.src(config.css.texteditor)
    .pipe(plumber({errorHandler: notify.onError({title:"css-texteditor error", message: "<%= error.message %>"})}))
    .pipe(sass({
      errLogToConsole: false
    }))
    .pipe(prefix({
      browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3'],
			cascade: true,
			remove: true
		}))
    .pipe(replace('/*!', '/*'))
    .pipe(gulp.dest(config.css.dest))
    .pipe(notify({ message: 'Build texteditor.css styles complete' }));
});

// generate /js/libs.min.js
gulp.task('js-libs', function(next){

  var bowerfiles = mainBowerFiles({
    filter: '**/*.js',
    checkExistence: false,
    paths: config.bower,
    debugging: false
  });
  
  //add libs src files to libs
  bowerfiles.push(config.js.libs);
  
  bowerfiles.forEach(function(entry) {
      gutil.log(entry);
  });  
      
  return gulp.src(bowerfiles)
    .pipe(plumber({errorHandler: notify.onError({title:"js-libs error", message: "<%= error.message %>"})}))
    .pipe(concat('libs.min.js'))
    .pipe(uglify(config.uglify))
    .pipe(gulp.dest(config.js.dest))
    .pipe(notify({ message: 'Build libs.min.js complete' }));

});

// generate /js/main.min.js
gulp.task('js-main', function(next){    
  return gulp.src(config.js.main)
    .pipe(plumber({errorHandler: notify.onError({title:"js-main error", message: "<%= error.message %>"})}))
    .pipe(uglify(config.uglify))
    .pipe(rename('ignite.min.js'))
    .pipe(gulp.dest(config.js.dest))
    .pipe(notify({ message: 'Build ignite.min.js complete' }));
});